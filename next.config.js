/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  images:{
    domains : ['images.pexels.com','media.istockphoto.com','wallpapercave.com','b.zmtcdn.com'],
    remotePatterns : [
      {
      protocol: 'http',
      hostname: 'localhost',
      port: '1337',
      pathname: '',
    },
  ]
  }
}

module.exports = nextConfig


