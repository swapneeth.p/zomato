import Header from './components/Header';
import { useState, useEffect } from "react";
import axios from "axios";

const Orders = () => {

  const [data, setData] = useState();

  useEffect(() => {
    const fetchData = async () => {
      try {
        const res = await axios.get("http://localhost:1337/api/orders");
        const products = res.data.data;
        setData(products)
      } catch (err) {
        console.error(err);
      }
    };
    fetchData();
  }, []);

  return (
    <>
    <Header />
    <div style={{ padding: '2rem' }}>
      <div style={{ maxWidth: '70%', margin: '0 auto' }}>
        <h3 style={{ color: '#2D3748', fontSize: '2.5rem', fontWeight: 'bold', marginBottom: '1.5rem' }}>Order History</h3>
      </div>
          <div style={{ maxWidth: '70%', margin: '0 auto' }}>
            <table style={{ width: '100%', borderCollapse: 'collapse', border: '1px solid #CBD5E0' }}>
              <tbody>
                <tr style={{ backgroundColor: '#E2E8F0', color: '#2D3748', fontWeight: 'bold' }}>
                  <th style={{ padding: '0.75rem' }}>Product Name</th>
                  <th style={{ padding: '0.75rem' }}>Price</th>
                  <th style={{ padding: '0.75rem' }}>Quantity</th>
                  <th style={{ padding: '0.75rem' }}>Total Price</th>
                </tr>
                {data && data.map((item, index) => {
                  return (
                    <tr key={index} style={{ backgroundColor: index % 2 === 0 ? '#F7FAFC' : '#EDF2F7' }}>
                      <td style={{ padding: '0.75rem', textAlign: 'left' }}>{item.attributes.productname}</td>
                      <td style={{ padding: '0.75rem' }}>Rs : {item.attributes.productprice}/-</td>
                      <td style={{ padding: '0.75rem' }}>
                      {item.attributes.quantity}
                      </td>
                      <td style={{ padding: '0.75rem' }}>{item.attributes.totalprice}</td>
                    </tr>
                    );
                })}
            </tbody>
          </table>
        </div>
    </div>
    </>
  );
};
    
export default Orders;