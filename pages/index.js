import React, { useState } from 'react';
import axios from "axios";
import Index from '../pages/Product/index';
import styles from '../styles/words.module.css';
import Link from 'next/link';

function Login() {
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [loggedIn, setLoggedIn] = useState(false);
  const [user, setUser] = useState('')

  const handleLogin = async event => {
    event.preventDefault();
    try {
      const response = await axios.get(`http://localhost:1337/api/registers?username=${username}&email=${email}`);
      console.log(response.data.data);
  
      if (Array.isArray(response.data.data)) {
        const user = response.data.data.find(
          (user) => user.attributes.username === username && user.attributes.email === email
        );

        if (user) {
          setLoggedIn(true);
          setUser(user.attributes.username);
        }
      }
    } catch (error) {
      console.error(error.message);
      if (error.response) {
        console.error(error.response.data);
      }
    }
  };
  

  if (loggedIn) {
    return <Index user={user}/>;
  }
// C:\Routing\myapp\public\Food.png
  return (
    <div className={styles.background}>
    <div className={styles.container}>
      <div className={styles.body}>
        <form id={styles.form} onSubmit={handleLogin}>
          <h2 style={{textAlign:'center'}}>Zomato Login</h2>
          <div className={styles.formGroup}>
            <label>Email</label>
            <input type="email" className="form-control" name='email' value={email} onChange={(event) => setEmail(event.target.value)}/>
          </div>
          <div className={styles.formGroup}>
            <label>Password</label>
            <input type="password" className="form-control" name='username' value={username} onChange={(event) => setUsername(event.target.value)}/>
          </div>
          <div className={styles.submit}>
            <button className="btn btn-primary">Login</button>
          </div>
          <div style={{float:'right'}}>
            Don't have an account? <Link href="/register">Sign up</Link>
          </div>
        </form>
      </div>
    </div>
  </div>
  );
}

export default Login;
