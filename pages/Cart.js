import React from 'react';
import { useCart } from 'react-use-cart';
import Header from './components/Header';
import axios from 'axios';
import styles from '../styles/words.module.css';
import { useState } from 'react';
import swal from 'sweetAlert';
import Link from 'next/link';

export default function Cart() {
  const [productName, setProductName] = useState();
  const [productPrice, setProductPrice] = useState();
  const [quantity, setQuantity] = useState();
  const [totalPrice, setTotalPrice] = useState();

  const {
    isEmpty,
    totalUniqueItems,
    items,
    totalItems,
    cartTotal,
    updateItemQuantity,
    removeItem,
    emptyCart,
  } = useCart();

  const handlePlaceOrder = async () => {
    try {
      const order = {
        data: {
          productname : productName,
          quantity : quantity,
          productprice: productPrice,
          totalprice: totalPrice,
        },
      };
      const response = await axios.post(
        'http://localhost:1337/api/orders',
        order
      );
      console.log(response.data);
      swal("Success!", "Order Placed Successfully!", "success");
      emptyCart();
    } catch (error) {
      console.error(error);
    }
  };

  const handleQuantityChange = (itemId, newQuantity) => {
    const item = items.find((item) => item.id === itemId);
    if (item) {
      const itemname =item.name;
      const itemPrice = item.price;
      const newTotalPrice = itemPrice * newQuantity;
      setProductName(itemname);
      setProductPrice(itemPrice);
      setQuantity(newQuantity);
      setTotalPrice(newTotalPrice);
      updateItemQuantity(itemId, newQuantity);
    }
  };

  if (isEmpty)
    return (
      <div>
          <div
            style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              minHeight: '90vh',
            }}
          >
            <h3>
              Hey it feels so light! Your Cart is empty, Lets add some items in
              cart
            </h3>
          </div>
          <div style={{
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
              }}>
            <div>
              <Link href="/Product" className='btn'>Back to Home</Link>
            </div>
            <div>
              <Link href="/Orders" className='btn'>Go to Orders</Link>
            </div>
          </div>
      </div>
    );
  return (
    <>
      <Header />
      <div className="container p-1 mx-auto" style={{maxWidth:'70%'}}>
      <section className="py-4 container-fluid">
        <div className="row justify-content-center">
          <div className='mt-1 mb-2'>
            <h2>
              Cart ({totalUniqueItems}) <span style={{float:'right'}}>Total Items : ({totalItems})</span>)
            </h2>
          </div>
          <div className="col-12">
            <table className="table table-light table-hover m-0">
              <tbody>
                <tr>
                  <th>Product Image</th>
                  <th>Product name</th>
                  <th>Price</th>
                  <th>Quantity</th>
                  <th></th>
                </tr>
                {items.map((item, index) => {
                  return (
                    <tr key={index}>
                      <td>
                        <img
                          src={item.image}
                          height={70}
                          width={90}
                          alt="Product"
                        ></img>
                      </td>
                      <td>{item.name}</td>
                      <td>Rs : {item.price}/-</td>
                      <td>
                        <button
                          className="btn btn-danger btn-sm psm"
                          onClick={() =>
                            handleQuantityChange(item.id, item.quantity - 1)
                          }
                        >
                          -
                        </button>
                        <span className="btn btn-sm btn-outline-danger">
                          {item.quantity}
                        </span>
                        <button
                          className="btn btn-danger btn-sm psm"
                          onClick={() =>
                            handleQuantityChange(item.id, item.quantity + 1)
                          }
                        >
                          +
                        </button>
                      </td>
                      <td>
                        <button
                          className="btn btn-danger ms-2 btn-sm"
                          onClick={() => removeItem(item.id)}
                        >
                          Remove Item
                        </button>
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <br></br>
            <div className="col-auto  mr-auto" style={{ float: 'right' }}>
              <h2>Total Price : {cartTotal}</h2>
                            
                    <button className="btn btn-success m-2" onClick={handlePlaceOrder}
                                    >
                                    Pay Now
                    </button>



                                
                            </div>
                        </div>
                    </div>
                    
                </section>
                </div>
            </>
  );
}

