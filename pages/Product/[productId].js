import { useRouter } from "next/router";
import Header from "../components/Header";
import Image from "next/image";
import styles from '../../styles/words.module.css'
import axios from "axios";

const ProductDetail = ({ words }) => {
  const router = useRouter();
  const productId = router.query.productId;

  const filteredWords = words.filter(word => word.name === productId);
//   console.log(filteredWords)

  if (!filteredWords.length) {
    return <div>Product not found</div>
  }

  return (
   <>
      <Header totalItems={totalItems}/>
      <div className="container p-2 mt-3">
        <div className="row">
          <div className="row row-cols-3">
            <div className="card">
              <div className="card-body">
                <Image src={filteredWords[0].image} width={315} height={230}></Image>
                <div className="card-title mt-2" id={styles.ctitle}><b>{filteredWords[0].name}</b></div>
                <div className="mb-2"><b>Price : {filteredWords[0].price}</b></div>
                <p>{filteredWords[0].name} dish is a delicious and satisfying combination of flavors and textures. It features a variety of fresh and cooked ingredients, including vegetables, meats or proteins, herbs and spices, and sauces or dressings. With every bite, you'll enjoy a burst of deliciousness that will leave you feeling nourished and satisfied. Whether you're looking for a quick and easy weeknight dinner or a special meal to share with friends and family, this dish is sure to please.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export const getServerSideProps = async() => {
  const res = await axios.get('http://localhost:1337/api/items?populate=*');
  const data = await res.json();
  return {
    props: { words : data }
  }
}

export default ProductDetail;
